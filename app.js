var scores, roundScore, activePlayer, gamePlaying, sixes, winScore;

function init(){
	scores = [0,0];
	activePlayer = 0;
	roundScore = 0;
	gamePlaying = true;
	sixes = 0;

    document.getElementById('dice-1').style.display = 'none';
    document.getElementById('dice-2').style.display = 'none';
	document.getElementById('score-0').textContent = '0';
	document.getElementById('score-1').textContent = '0';
	document.getElementById('current-0').textContent = '0';
	document.getElementById('current-1').textContent = '0';
	document.getElementById('win-score').value = '100';
	document.getElementById('name-0').textContent = 'Player 1';
	document.getElementById('name-1').textContent = 'Player 2';
	document.querySelector('.player-0-panel').classList.remove('winner');
	document.querySelector('.player-1-panel').classList.remove('winner');
	document.querySelector('.player-0-panel').classList.remove('active');
	document.querySelector('.player-1-panel').classList.remove('active');
	document.querySelector('.player-0-panel').classList.add('active');
}

function nextPlayer(){
	// next player
	activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
	roundScore = 0;

	document.getElementById('current-0').textContent = '0';
	document.getElementById('current-1').textContent = '0';

	document.querySelector('.player-0-panel').classList.toggle('active');
	document.querySelector('.player-1-panel').classList.toggle('active');

   // document.getElementById('dice-1').style.display = 'none';
   // document.getElementById('dice-2').style.display = 'none';
}

init();

document.querySelector('.btn-roll').addEventListener('click', function(){

	if(gamePlaying){
		// random number
		var dice1 = Math.floor(Math.random() * 6) + 1;
		var dice2 = Math.floor(Math.random() * 6) + 1;

		// display result
        document.getElementById('dice-1').style.display = 'block';
        document.getElementById('dice-2').style.display = 'block';
        document.getElementById('dice-1').src = 'dice-' + dice1 + '.png';
        document.getElementById('dice-2').src = 'dice-' + dice2 + '.png';

		//dice1 === 6 || dice2 === 6 ? sixes += 1 : sixes = 0;

		// update the round score IF the rolled number is NOT 1 OR there is NOT 2 sixes in row
		if(dice1 !== 1 && dice2 !== 1){
			// add score
			roundScore += dice1 + dice2;
			document.querySelector('#current-' + activePlayer).textContent = roundScore;
		}else{
			// next player
			nextPlayer();
		}
	}
});

document.querySelector('.btn-hold').addEventListener('click', function(){
	
	if (gamePlaying) {
		// get winning score value
		winScore = document.getElementById('win-score').value;

		// add current score to global score
		scores[activePlayer] += roundScore;
		
		// update the UI
		document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

		// check if player won the game
		if(scores[activePlayer] >= winScore){
			document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.getElementById('dice-1').style.display = 'none';
            document.getElementById('dice-2').style.display = 'none';
			document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
			document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
			gamePlaying = false;
		}else{
			// next player
			nextPlayer();
		}
	}
});

document.querySelector('.btn-new').addEventListener('click', init);