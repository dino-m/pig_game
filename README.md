#Pig Game

This is a simple application that simulates popular Pig Game written in JavaScript.

###GAME RULES:

- The game has 2 players, playing in rounds
- First thing to do is set the winning score (by default it is 100)
- In each turn, a player rolls a dices as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLOBAL score. After that, it's the next player's turn
- Winner is the player who gets first to the winner score